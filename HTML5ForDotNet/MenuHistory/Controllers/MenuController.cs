﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MenuHistory.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/
        public ActionResult Index(string meal="",string dish="")
        {
            var Menu = new Models.Menu();
            Menu.SelectedMeal = meal;
            Menu.Items.Where(s => s.Meal == meal && s.Key == dish).FirstOrDefault().IsSelected = true;
            return View(Menu);
        }
	}
}