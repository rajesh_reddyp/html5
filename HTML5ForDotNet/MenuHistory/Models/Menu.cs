﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MenuHistory.Models
{
    public class Menu
    {
        public List<MenuItem> Items { get; set; }
        public string SelectedMeal { get; set; }
    }
}