﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MenuHistory.Models
{
    public class MenuItem
    {
        public string DishDescription { get; set; }
        public string DishName { get; set; }
        public bool IsSelected { get; set; }
        public string Key { get; set; }
        public string Meal { get; set; }
    }
}